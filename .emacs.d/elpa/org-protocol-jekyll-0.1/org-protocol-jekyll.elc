;ELC   
;;; Compiled by thattommyhall@Beast on Mon Jul 22 19:59:17 2013
;;; from file /home/thattommyhall/.emacs.d/elpa/org-protocol-jekyll-0.1/org-protocol-jekyll.el
;;; in Emacs version 24.2.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(require 'org-protocol)
#@1131 Map URLs to local filenames for `org-protocol-jekyll' (jekyll).

Each element of this list must be of the form:

  (module-name :property value property: value ...)

where module-name is an arbitrary name.  All the values are strings.

Possible properties are:

  :base-url          - the base URL, e.g. http://www.example.com/project
  :permalink         - the permalink to generate URLs for the site
  :working-directory - the local working directory. This is, what
                       base-url will be replaced with.
  :working-suffix    - acceptable suffixes for the file converted to
                       HTML. Can be one suffix or a list of suffixes.

Example:

  (setq org-protocol-jekyll-alist
        '(("Jekyll's awesome website."
           :base-url "http://jekyllrb.com"
           :permalink "pretty"
           :working-directory "/home/user/jekyll"
           :working-suffix (".md", ".markdown"))
          ("Local Jekyll's site."
           :base-url "http://localhost:4000"
           :permalink "pretty"
           :working-directory "/home/user/jekyll"
           :working-suffix (".md", ".markdown"))))
(custom-declare-variable 'org-protocol-jekyll-alist 'nil '(#$ . 582) :group 'org-protocol :type 'alist)
(add-to-list 'org-protocol-protocol-alist '("org-jekyll" :protocol "jekyll" :function org-protocol-jekyll))
#@206 Process an org-protocol://jekyll:// style url.

The location for a browser's bookmark should look like this:

  javascript:location.href='org-protocol://jekyll://'+ \
    encodeURIComponent(location.href)
(defalias 'org-protocol-jekyll #[(data) "\304!\211\305\306\307	\"O\310\311\312\215+\207" [data url url1 fname org-link-unescape 0 string-match "\\([\\?#].*\\)?$" nil result (byte-code "\306\211\203u \n@\307	A\310\"\311\312!\313Q\314\f\"\203m \315\316\317\211$!\320\307	A\321\"!\"\307	A\322\"\211#<\203A #\202D #C\323\324\325\317\326$\210\314\327\"\"\203\\ \"\330\331O\"\332\333\334\335	!\"\336	!\"\"\"\210,*\nA\211\204 *\337\340!\210\306\207" [org-protocol-jekyll-alist blog #1=#:--cl-dolist-temp-- base-url base-re url1 nil plist-get :base-url "^" regexp-quote "/?" string-match replace-match "/" t expand-file-name :working-directory :working-suffix add-to-list suf-list ".html" string= "/$" 0 -1 mapc #[(file) "\306\211\2057 \n@\f	Q\307!\203 \310\311\"\210\202. \312!\203. \313\314\"\210\310\311\306\"\210)\nA\211\204 \306*\207" [suf-list add-suffix #2=#:--cl-dolist-temp-- wdir file path nil file-readable-p throw result file-exists-p message "%s: permission denied!"] 4] append org-protocol-jekyll-posts org-protocol-jekyll-pages message "No corresponding source file." site-url wdir suf] 8)] 6 (#$ . 1931)])
#@188 Build a list of candidates for the source file of the ordinary
page with PAGE-URL (for post URL's, see
`org-protocol-jekyll-posts'). The working directory and file
extension are avoided.
(defalias 'org-protocol-jekyll-pages #[(blog page-url) "\304A\305\"\306\307	\"\n\203 	\310\nOC\202, \311\230\205, \306\312	\"\205, 	\310\313O	\314PD*\207" [blog page-url end-pos permalink plist-get :permalink string-match "\\.html$" 0 "pretty" "/$" -1 "index"] 4 (#$ . 3297)])
#@188 Build a list of candidates for the source file of the post
with POST-URL (for ordinary-page URL's, see
`org-protocol-jekyll-pages'). The working directory and file
extension are avoided.
(defalias 'org-protocol-jekyll-posts #[(blog post-url) "\306A\307\"\206	 \310\311\211\312\313\f\"\203 \f\314P\310\230\203# \315\202O \316\230\203- \317\202O \320\230\2037 \321\202O \322\230\203A \323\202O \312\313\"\203N \314P\202O \324\f\"\206c \324\f\325\326\327\325\330\331##\"\211\205\241 \n@\nA@\332\n8\333P	B\334\335R	B\203\237 \327\333R	B\327\334\335\260	B+	+\207" [blog file-list props permalink post-url title plist-get :permalink "date" nil string-match "/$" "index.html" "/:categories/:year/:month/:day/:title.html" "none" "/:categories/:title.html" "ordinal" "/:categories/:year/:y_day/:title.html" "pretty" "/:categories/:year/:month/:day/:title/index.html" org-protocol-jekyll-match replace-regexp-in-string "//" "/" ":categories" "" 2 "/_drafts/" "/_posts/" "-" date categories] 10 (#$ . 3773)])
#@126 Match POST-URL to PERMALINK. Return a list containing the categories,
the date and the title for POST-URL, or nil otherwise.
(defalias 'org-protocol-jekyll-match #[(post-url permalink) "\306\307\310\311\312#\313Q\314\315\316\n	\"\"\317!S\320\310\321	#\322Q\323\211()\323\211*+\323\211,-\324\n\"\210\f\325././W\203\203 \f.Z0\3260\n\"1\3271\"A20831(B(3)B)\3302\313Q)B),.T\211.\202@ *@)B)\331\332\320\333)\334\"B\"*\324*4\"\205;\f\325.5.5W\203\304 \326\f.Z4\"+B+.T\211.\202\250 *\335(+\",\323\211\211\211\211\211\211\211\211\2116789:;<=>?\336\337,\"\2109\340\341\342?!>\203\342>!\202 :\203\342:!\202 8\205 \343\3448!\345\346\347$T=\203,\342=!\2025;\2055\342;!$<E.\n.\207" [template token-re permalink slices len link-re (("year" . "[0-9]\\{4\\}") ("month" . "[0-9]\\{2\\}") ("day" . "[0-9]\\{2\\}") ("title" . "[^/]+") ("i_day" . "[0-9]\\{1,2\\}") ("i_month" . "[0-9]\\{1,2\\}") ("categories" . "\\(?:[^/]+/\\)*[^/]+") ("short_month" . "[a-zA-Z]\\{3,4\\}") ("y_day" . "[0-9]\\{3\\}") ("output_ext" . "\\.[a-zA-Z]+")) ":\\(" mapconcat car "\\|" "\\)" mapcar regexp-quote split-string list-length "^" identity "$" nil string-match 0 match-string assoc "\\(" apply concat append ("$") pairlis mapc #[(c) "\301@!AL\207" [c intern] 2] format "%04d-%02d-%02d" string-to-number position capitalize ("Jan" "Feb" "Mar" "Apr" "May" "June" "July" "Aug" "Sept" "Oct" "Nov" "Dec") :test string= tokens re-list url-re values tok-alist val-list i #1=#:--cl-dotimes-temp-- n tok re str post-url #2=#:--cl-dotimes-temp-- output_ext y_day short_month categories i_month i_day title day month year] 11 (#$ . 4819)])
(provide 'org-protocol-jekyll)
