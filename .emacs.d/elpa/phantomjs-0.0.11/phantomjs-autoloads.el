;;; phantomjs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (phantomjs-server phantomjs-run) "phantomjs" "phantomjs.el"
;;;;;;  (20973 33008))
;;; Generated autoloads from phantomjs.el

(autoload 'phantomjs-run "phantomjs" "\


\(fn NAME CALLBACK &rest SCRIPTS)" t nil)

(autoload 'phantomjs-server "phantomjs" "\


\(fn NAME PORT COMPLETE-CALLBACK)" t nil)

;;;***

;;;### (autoloads nil nil ("phantomjs-pkg.el") (20973 33008 434997))

;;;***

(provide 'phantomjs-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; phantomjs-autoloads.el ends here
